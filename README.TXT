

Forma correcta para la utilización del programa:
	compilación: gcc lab01.c -o lab01 -lgmp
	Ejecución: ./lab01 a b c d e
	donde a = p, algún entero positivo que representará uno de los números primos largos elegidos por Alice
	      b = q, Algún entero positivo que representará el segundo número primo largo elegido por Alice
	      C = e, algún entero positivo que representará un nuevo número aleatorio elegido por Alice
	      d = m, algún entero positivo que será elevado por el siguiente número ingresado
	      e = d, algún entero positivo que elevará al número anterior ingresado.

Una vez ingresados los valores correctamentes por la terminal, se preguntará si desea realizar o no la factorización, ya que ésta tiene una limitante para la correcta ejecución de la función "factorizar". 
La limitante es que para los p y q ingresados, la multiplicación resultante de éstos, no debe exceder los 90.000
Para poder ver correctamente el funcionamiento de las otras funciones del programa, se debe ingresar la opción para omitir la factorización.

Por lo tanto, el programa mostrará el siguiente texto en la terminal:
"Desea realizar la factorización ?, recuerde que para valores de entradaen los que P * Q > 90.000, el programa no funciona correctamente. Introduzca 1, para ver la factorización.\nIntroduzca 0, para NO ver la factorización"

Por lo que si se desea ver la factorización, se debe ingersar un 1, a través de la terminal y si no. se debe ingresar un 0.