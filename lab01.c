/*
	Compilado en MacOs Mojave 10, Debian 9 y terminal de Ubuntu instalada en windows 10.
	en todos los casos se utilizó para compilar GCC con la librería GMP

	Autores: Felipe Parra, Fernando García.

	Forma correcta para la utilización del programa:
	compilación: gcc lab01.c -o lab01 -lgmp
	Ejecución: ./lab01 a b c d e
	donde a = p, algún entero positivo que representará uno de los números primos largos elegidos por Alice
	      b = q, Algún entero positivo que representará el segundo número primo largo elegido por Alice
	      C = e, algún entero positivo que representará un nuevo número aleatorio elegido por Alice
	      d = m, algún entero positivo que será elevado por el siguiente número ingresado
	      e = d, algún entero positivo que elevará al número anterior ingresado.
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <gmp.h>
#include <time.h>

void multiplicacion(char *numsEntrada[]);
void gcdAlice (char *numsEntrada[]);
void factorizacion(char *numsEntrada[]);
void getBinario(char *numsEntrada[]);
void exponenciacionModular(char* numsEntrada[]);

int main(int argc, char *argv[])
{		

	if(argc != 6)
	{
		printf("Se ingresaron mal los valores a través del programa \nConsidere lo siguiente:\n");
		printf("\tEjecución: ./lab01 a b c d e\n");
		printf("donde a = p, algún entero positivo que representará uno de los números primos largos elegidos por Alice\n");
		printf("\tb = q, Algún entero positivo que representará el segundo número primo largo elegido por Alice\n");
		printf("\tC = e, algún entero positivo que representará un nuevo número aleatorio elegido por Alice\n");
		printf("\td = m, algún entero positivo que será elevado por el siguiente número ingresado\n");
		printf("\te = d, algún entero positivo que elevará al número anterior ingresado.\n");
		return 0;
	}

/*
Varibles para medicion de tiempo.
*/
	clock_t start_t, end_t, total_t;
	int option;

	printf("\nDesea realizar la factorización ?, recuerde que para valores de entrada\nen los que P * Q > 90.000, el programa no funciona correctamente.\n");
	printf("\nIntroduzca 1, para ver la factorización.\nIntroduzca 0, para NO ver la factorización\n");
	scanf("\n%d", &option);
/*
C o m i e n z o  e n u n c i a d o  (A)
*/
	start_t = clock();
	multiplicacion(argv);
	end_t = clock();
	printf("\n\tTiempo empleado: %f", (double)(end_t - start_t) / CLOCKS_PER_SEC);
/*
C o m i e n z o  e n u n c i a d o  (B)
*/
	start_t = clock();
	gcdAlice(argv);
	end_t = clock();
	printf("\n\tTiempo empleado: %f", (double)(end_t - start_t) / CLOCKS_PER_SEC);
/*
C o m i e n z o  e n u n c i a d o (C)
*/
	start_t = clock();
	exponenciacionModular(argv);
	end_t = clock();
	printf("\n\tTiempo empleado: %f", (double)(end_t - start_t) / CLOCKS_PER_SEC);
/*
C o m i e n z o  e n u n c i a d o  (D)
*/
	if (option == 1)
	{
		start_t = clock();
		factorizacion(argv);
		end_t = clock();
		printf("\n\tTiempo empleado: %f", (double)(end_t - start_t) / CLOCKS_PER_SEC);
	}

	printf("\n\nFin Programa\n");
	return 0;
	
}

void getBinario(char *numsEntrada[])
{	
	char * numSalida;
	mpz_t numIn;
	mpz_init(numIn);
	mpz_set_str(numIn, numsEntrada[5], 10);
	//mpz_out_str(stdout, 10, numIn);
	mpz_get_str(numsEntrada[5], 2, numIn);
	//printf("\n\tel nuevo valor de argv[5]: %s\n", numsEntrada[5]);
	return;
}

void gcdAlice (char *numsEntrada[])
{
	printf("\n\nAlgoritmo de Euclides:\n");
	mpz_t numIn, numIn2, e, val0;

	//Creación de b, ya que se calculará gcd(a,b) donde a = e y b = (p-1)(q-1).
	//En este caso p y q, son ingresados y se almacenan en argv[1] y argv[2] respectivamente.
	mpz_init(numIn);
	mpz_set_str(numIn, numsEntrada[1], 10);
	mpz_sub_ui(numIn, numIn, 1);
	

	mpz_init(numIn2);
	mpz_set_str(numIn2, numsEntrada[2], 10);
	mpz_sub_ui(numIn2, numIn2, 1);
	mpz_mul(numIn2, numIn, numIn2);
	
	
	//Creación de "e", falta verificar si éste se ingresa, o se crea aleatoriamente
	mpz_init(e);
	mpz_set_str(e, numsEntrada[3], 10);

	//valor en 0 para correcto uso del while.
	mpz_init(val0);
	mpz_set_str(val0, "0",10);

	printf("\tNúmeros modificados de la forma gcd(e, (p-1)(q-1)): ");
	mpz_out_str(stdout,10, e);
	printf(", ");
	mpz_out_str(stdout, 10, numIn2);
	printf("\n\tGCD = ");


	while (mpz_cmp(numIn2, val0) != 0)
	{
		//desde aquí, numIn será utilizado como auxiliar.
		mpz_mod(numIn, e, numIn2);
		mpz_set(e, numIn2);
		mpz_set(numIn2, numIn);
	}
	mpz_out_str(stdout, 10, e);

	// Liberación de memoria
	mpz_clear(numIn); mpz_clear(numIn2); mpz_clear(e); mpz_clear(val0);
	return;
}

void factorizacion(char *numsEntrada[])
{
	printf("\n\nFactorización de n:\n");

	mpz_t *P;
	mpz_t *Q;
	mpz_t n;
	mpz_t n_sqrt;
	mpz_init(n);
	mpz_init(n_sqrt);
	// n se genera de la multiplicación de p * q, en este caso argv[1] * argv[2]
	mpz_set_str(n,numsEntrada[1],10);
	mpz_mul_ui(n, n, atoi(numsEntrada[2]));
	printf("\nN = ");
	mpz_out_str(stdout, 10, n);
	printf("\n");
	mpz_sqrt(n_sqrt,n);
	unsigned long int i, j, aux1, aux2;

	// initialization of P 
	P = (mpz_t *) malloc(100000 * sizeof(mpz_t));
	if (NULL == P) {
	    printf("ERROR: Out of memory\n");
	    return;
	}

	for(i=0; mpz_cmp_ui(n, i) > 0; i++){
		mpz_init_set_ui(P[i], 1);		//Se setea P[i] = 1
	}

	clock_t start_t, end_t, total_t;
	start_t = clock();
	mpz_set_ui(P[0], 0);
	for(i=1; mpz_cmp_ui(n_sqrt,i); i++){	//Se aplica el algoritmo Sieve of Eratosthenes
		aux1 = i + 1;
		if(P[i]){
			aux2 = aux1 * aux1;
			for(j=aux2; mpz_cmp_ui(n, j) >= 0; j+=aux1){
				mpz_set_ui(P[j-1], 0);
			}
		}
	}

	Q = (mpz_t *) malloc(1000000 * sizeof(mpz_t));

	int k=0;
	for(i=0; mpz_cmp_ui(n, i) > 0; i++) {
		if(!mpz_cmp_ui(P[i], 1)){
			mpz_init_set_ui(Q[k], i+1);
			k++;
		}	
	}
	free(P);

	mpz_t *factors;
	factors = (mpz_t *) malloc(1000 * sizeof(mpz_t));
	mpz_t aux;
	mpz_init_set_ui(aux,0);
	int t=0;
	for(int i=0; mpz_cmp_ui(n_sqrt,i) > 0; i++)
	{
		mpz_mul(aux,Q[i], Q[i]);
		if(mpz_cmp(aux,n) > 0)
			break;
		mpz_mod(aux, n,Q[i]);
		while(mpz_cmp_ui(aux,0) == 0)
		{ 
			mpz_init(factors[t]);
			mpz_set(factors[t], Q[i]);
			t++;
			mpz_tdiv_q(n, n,Q[i]);
			mpz_mod(aux, n,Q[i]);
		}
	}
	if(mpz_cmp_ui(n,1) > 0)
	{
		mpz_init(factors[t]);
		mpz_set(factors[t], n);
		t++;
	}
	mpz_clear(aux);
	printf("\tLos factores son :");
	int freq = 1;
	for(int i=0; i<t; i++)
	{
		if(mpz_cmp( factors[i] , factors[i+1]) == 0 )
		 	freq++;
		else{
			printf("(");
			mpz_out_str(stdout, 10, factors[i] );
			printf(" , %i) ", freq );
			freq=1;
		}
	}

}

void multiplicacion(char *numsEntrada[])
{
	printf("\n\nMultiplicación de 2 números de entrada:\n");
	mpz_t numIn;
	mpz_t numIn2;
	mpz_t resultadoMultiplicacion;

	mpz_init(numIn);
	mpz_set_str(numIn, numsEntrada[1], 10);

	mpz_init(numIn2);
	mpz_set_str(numIn2, numsEntrada[2], 10);

	mpz_init(resultadoMultiplicacion);
	mpz_mul(resultadoMultiplicacion, numIn, numIn2);
	printf("\tNúmeros ingresados: ");
	mpz_out_str(stdout, 10, numIn);
	printf(", ");
	mpz_out_str(stdout, 10, numIn2);
	printf(".\n\tMultiplicación = ");
	mpz_out_str(stdout, 10, resultadoMultiplicacion);

	printf("\n");

	return;
}

void exponenciacionModular(char* numsEntrada[])
{
	printf("\n\nExponenciacion Modular:");

	mpz_t numIn;
	mpz_init(numIn);
	mpz_set_str(numIn, "1", 10);

	int i;

	mpz_t numAux;
	mpz_init(numAux);
	mpz_set_str(numAux, numsEntrada[4], 10);
	printf("\n\tnumEntrada. m = ");
	mpz_out_str(stdout, 10, numAux);
	printf("\n\tnumPotencia d = %s", numsEntrada[5]);
	//en getBinario, se transforma el n ingresado en argv[5], por un mpz con el número que luego se transformó a binario y se 
	//guardó como string para una correcta utilización.
	getBinario(numsEntrada);

	
	printf("\n\tnumPotencia(binario) d = ");
	printf("%s\n", numsEntrada[5]);
	//printf("\n\tLargo num binario: %lu\n", strlen(argv[5]));

	for (i = 0; i < strlen(numsEntrada[5]); i++)
	{
		mpz_mul(numIn, numIn, numIn);
		if (numsEntrada[5][i] == '1')
		{
			mpz_mul(numIn, numIn, numAux);
		}
	}
	printf("\t"); /*mpz_out_str(stdout, 10, numAux); printf(" elevado a "); printf("%s", argv[5]);*/
	printf("m^d = "); mpz_out_str(stdout, 10, numIn);
}
